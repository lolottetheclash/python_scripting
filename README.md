strextract

Mots-clés, commandes : argparse, /usr/local/bin, re, walk

Ecrire un programme Python 3 qui parcours récursivement tous les fichiers d’une hiérarchie et produit en sortie toutes les chaînes littérales (= les textes entourés par " ou ’) que contiennent ces fichiers.

Les chaînes seront affichées sous la forme où elles étaient dans le fichier, entourées des même symboles " ou ’.
Par exemple, si le contenu d’un des fichiers parcouru est:

<v-text-field
   label="Nom d'utilisateur"
   v-model='user.username'
   :disabled="userId > 0" :error-messages="usernameErrors"
   @input="$v.user.username.$touch(); $v.user.$touch()"
></v-text-field>

Ce fichier conduira à la production des lignes suivantes sur la sortie standard stdout :

"Nom d'utilisateur"
'user.username'
"userId > 0"
"usernameErrors"
"$v.user.username.$touch(); $v.user.$touch()"
On ne cherchera pas à capturer les chaînes multi-lignes.

strextract [options] <dir>

Le seul argument obligatoire de la commande est dir, chemin d’accès au répertoire à partir duquel est faite la recherche

Les options de la commande sont :
-h, –help : affiche l’usage de la commande
–suffix=suffix : limite la recherche aux fichiers de suffixesuffix
–path: chaque ligne produite est précédée du chemin d’accès au fichier associé suivi d’une tabulation
-a, –all: les fichiers cachés (ceux dont le nom commence par ‘.’) sont également inclus
Le programme doit être installé comme une commande Unix accessible à tous les utilisateurs
Exemple:

$ strextract --path --suffix vue .
...
/home/chris/myproject/User.vue<tab>"Nom d'utilisateur"
/home/chris/myproject/User.vue<tab>'user.username'
/home/chris/myproject/User.vue<tab>"userId > 0"
...




brklnk


Mots-clés, commandes : requests, beautifulsoup
Ecrire une commande brklnk en Python 3 qui parcoure tous les liens contenus dans une page web et affiche les liens cassés.

brklnk [options] <url>

Le programme doit être installé comme une commande Unix accessible à tous les utilisateurs

La commande doit avoir les options suivantes :
–help : affiche l’usage de la commande
–depth=n : profondeur de recherche n (1 par défaut)

Un lien qui n’a pas de nom de domaine, (ex: “domicile.html”) doit être accédé avec le protocole et le domaine de l’url courant

Le site web https://educ-a-dom.fr/brklnk pourra être utilisé pour des tests