#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import argparse
import re

def string_extraction(arguments):
    ''' Function which will look into the files which belongs to the repertory path, and display all literal strings which are between quotes or double quotes.'''
    between_quotes = re.compile((r"\"(.*)\"|\'(.*)\'"))
    for dirpath, _, filenames in os.walk(arguments.repertory_path):
        for filename in filenames:
            if arguments.suffix:
                if not filename.endswith(args.suffix):
                    continue
            if not arguments.all:
                if filename.startswith('.'):
                    continue
            my_file = os.path.join(dirpath, filename)
            try : 
                with open(my_file, 'r', encoding="utf8", errors='ignore') as fichier:
                    for line in fichier:
                        match = re.search(between_quotes, line)
                        if match:
                            if arguments.path:
                                print(f'{my_file}\t{match.group(0)}')
                            else:
                                print(match.group(0))
            except:
                pass


def main():
    global args
    if __name__ == '__main__':
        parser = argparse.ArgumentParser() 
        parser.add_argument('repertory_path', help="The path where you want to begin you research.")
        parser.add_argument('-a', '--all', help="This option add hidden files in your research.", action="store_true")
        parser.add_argument('--path', help="This option will display the path of the file which contains the string.", action="store_true")
        parser.add_argument('--suffix', help="This option will limit the research to the files which ends with this suffix")
        args = parser.parse_args()
        string_extraction(args)
main()




