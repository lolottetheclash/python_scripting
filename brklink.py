#! /usr/bin/env python3
from bs4 import BeautifulSoup
import requests
import argparse
import re
from urllib.parse import urljoin, urlparse

def broken_link(url, depth):
    '''Check if there is some broken links on the url page, depth option to look deeper in the search tree'''
    if depth < 0:
        return
    result = requests.get(url) 
    if requests.get(url).status_code >= 400:
        print(f'The link: {url} is broken.')
    else:
        src = result.content 
        soup = BeautifulSoup(src, 'lxml') 
        links = soup.find_all("a")
        for link in links:
            parsed_link = urlparse(link.get('href'))
            if parsed_link.scheme == '':
                link = urljoin(url, parsed_link.path)
            broken_link(link, depth-1)

def main():
    global args
    if __name__ == '__main__':
        parser = argparse.ArgumentParser("This command will allow you to check every links from the url you gave.")
        parser.add_argument('url', type=str, help="The url where you will check if it has some broken links.")
        parser.add_argument("--depth", type=int, help="Describe until which depth you want to analyse the links.")
        args = parser.parse_args()
        broken_link(args.url, args.depth) 
main()
